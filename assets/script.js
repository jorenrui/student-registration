function displayInfo() {
	var message = '<h1>SPCC Registration Form</h1>';

	message += '<p>Entry Date: ' + document.getElementById('entry_date').value + '</p>';

	var status = document.getElementById('status1').checked ? 'Regular' : 'Transferee';
	message += '<p>Status: ' + status + '</p>';

	message += '<h2>A. PERSONAL PROFILE</h2>';

	message += '<p>Given Name: ' + document.getElementById('fname').value + '</p>';
	message += '<p>Middle Name: ' + document.getElementById('mname').value + '</p>';
	message += '<p>Surname: ' + document.getElementById('lname').value + '</p>';

	var course = document.getElementById('course1').checked ? 'BSIT' : document.getElementById('course2').checked ? 'ACT' : 'AHRM';
	message += '<p>Course: ' + course + '</p>';

	message += '<p>Date of Birth: ' + document.getElementById('birthdate').value + '</p>';
	message += '<p>Place of Birth: ' + document.getElementById('birth_place').value + '</p>';
	message += '<p>Gender: ' + document.getElementById('gender').value + '</p>';
	message += '<p>Nationality: ' + document.getElementById('nationality').value + '</p>';
	message += '<p>Religion: ' + document.getElementById('religion').value + '</p>';
	message += '<p>Civil Status: ' + document.getElementById('civil_status').value + '</p>';
	message += '<p>City Address: ' + document.getElementById('city_address').value + '</p>';
	message += '<p>Tel/Cell No/: ' + document.getElementById('city_contact').value + '</p>';
	message += '<p>Provincial Address: ' + document.getElementById('provincial_address').value + '</p>';
	message += '<p>Tel/Cell No.: ' + document.getElementById('provincial_contact').value + '</p>';
	message += '<p>Father\'s Name: ' + document.getElementById('father_name').value + '</p>';
	message += '<p>Father\'s Occupation: ' + document.getElementById('father_job').value + '</p>';
	message += '<p>Father\'s Tel/Cel No.: ' + document.getElementById('father_contact').value + '</p>';
	message += '<p>Mother\'s Name: ' + document.getElementById('mother_name').value + '</p>';
	message += '<p>Mother\'s Occupation: ' + document.getElementById('mother_job').value + '</p>';
	message += '<p>Mother\'s Tel/Cel No.: ' + document.getElementById('mother_contact').value + '</p>';
	message += '<p>Emergency Contact\'s Name: ' + document.getElementById('emergency_name').value + '</p>';
	message += '<p>Relationship: ' + document.getElementById('emergency_rel').value + '</p>';
	message += '<p>Contact No. Tel.: ' + document.getElementById('emergency_tel').value + '</p>';
	message += '<p>Contact No. Cel.: ' + document.getElementById('emergency_cel').value + '</p>';

	message += '<h2>B. EDUCATIONAL PROFILE</h2>';

	message += '<p>Primary (1-3): ' + document.getElementById('educ_primary').value + ' (' + document.getElementById('educ_primary_sy').value + ')</p>';
	message += '<p>Intermediate (4-6): ' + document.getElementById('educ_inter').value + ' (' + document.getElementById('educ_inter_sy').value + ')</p>';
	message += '<p>Secondary: ' + document.getElementById('educ_second').value + ' (' + document.getElementById('educ_second_sy').value + ')</p>';

	message += '<h3>Transferee</h3>';

	message += '<p>College/University Last Attended: ' + document.getElementById('trans_school').value + '</p>';
	message += '<p>Course: ' + document.getElementById('trans_course').value + '</p>';
	message += '<p>Reason for Leaving: ' + document.getElementById('trans_reason').value + '</p>';

	message += '<h2>C. WORKING STUDENT-APPLICANT\'S PROFILE</h2>';

	message += '<p>Company: ' + document.getElementById('company').value + '</p>';
	message += '<p>Address: ' + document.getElementById('work_address').value + '</p>';
	message += '<p>Schedule: ' + document.getElementById('work_sched').value + '</p>';

	message += '<h2>D. MARKETING PROFILE: What/Who influenced you to enroll at SPCC?</h2>';

	var marketing = document.getElementById('market1').checked ? 'Career Orientation' : document.getElementById('market2').checked ? 'SPCC Leaflet/Poster' : document.getElementById('referral').value;

	message += '<p>' + marketing + '</p>';;

	document.write(message);
}